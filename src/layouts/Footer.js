import React from "react";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <div className="main-footer">
      <span>&copy; 2023. AutomationPro-X. All Rights Reserved.</span>
      <span>
        Created by:{" "}
        <Link to="https://fpt.vn/" target="_blank">
          Fpt
        </Link>
      </span>
    </div>
  );
}
